#!/usr/bin/env python

class Barcode(object):
    def __init__(self, x, y):
        super(Barcode, self).__init__()
        self.x = x
        self.y = y

    def __str__(self):
        return "{:>03}.{:>03}".format(self.x, self.y)

    def get_front(self):
        return Barcode(self.x-1, self.y)

    def get_right(self):
        return Barcode(self.x, self.y-1)

    def get_back(self):
        return Barcode(self.x+1, self.y)

    def get_left(self):
        return Barcode(self.x, self.y+1)

    def get_by_orientation(self, orientation=0):
        if orientation == 0:
            return self.get_front()
        elif orientation == 1:
            return self.get_right()
        elif orientation == 2:
            return self.get_back()
        elif orientation == 3:
            return self.get_left()

    @classmethod
    def from_string(cls, barcode):
        x,y = [int(data) for data in barcode.split(".")]
        return cls(x, y)

class Task(object):
    def __init__(self, cmd, comment=""):
        super(Task, self).__init__()
        self.cmd = cmd
        self.comment = comment

class Macro(object):
    def __init__(self):
        super(Macro, self).__init__()
        self.task = list()

    def add_task(self, task):
        self.task.append(task)

    def interactive(self):
        for task in self.task:
            if task.comment: print "\033[94m"+"%"+task.comment+"\033[0m"
            print task.cmd
            raw_input()

class ClearRightLine(Macro):
    def __init__(self, butler_id_list, top_barcode):
        super(ClearRightLine, self).__init__()
        if not butler_id_list:
            raise Exception("Empty butler list")
        self.butler_list = butler_id_list
        self.barcode = Barcode.from_string(top_barcode)
        # Generate macro
        barcode = self.barcode
        cmd = dict()
        for i,butler_id in enumerate(self.butler_list):
            self.add_task(Task(cmd="butler_test_functions:test_destination({}, {{\"{}\", 1}}).\n".format(butler_id, barcode),
                comment="Move below the rack"))
        for i,butler_id in enumerate(self.butler_list):
            self.add_task(Task(cmd="butler_test_functions:test_lift({}, 0).\n".format(butler_id),
                comment="Lift the rack"))
        for i,butler_id in enumerate(self.butler_list):
            self.add_task(Task(cmd="butler_test_functions:test_destination({}, {{\"{}\", 1}}).\n".format(butler_id, barcode.get_right()),
                comment="Move the rack"))
        for i,butler_id in enumerate(self.butler_list):
            self.add_task(Task(cmd="butler_test_functions:test_destination({}, {{\"{}\", 1}}).\n".format(butler_id, barcode),
                comment="Move the rack to original position"))
        for i,butler_id in enumerate(self.butler_list):
            self.add_task(Task(cmd="butler_test_functions:test_lift({}, 1).\n".format(butler_id),
                comment="Put rack down"))
            barcode = barcode.get_back()
        
            

if __name__ == '__main__':
    print Barcode(0,89)
    print Barcode.from_string("056.098")
    macro = ClearRightLine([1105,1116,1090,1091],"023.076")
    macro.interactive()
